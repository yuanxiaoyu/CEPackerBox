// =================================================================================================
//
//	CopyEngine Framework
//	Copyright 2012 Eran. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.utils.Dictionary;

	import copyengine.utils.packerbox.CEPackerBuilder;
	import copyengine.utils.packerbox.box.ICEPackerBox;
	import copyengine.utils.packerbox.data.CEPackerElementData;

	public class CEPackerBoxDemo extends Sprite
	{
		public function CEPackerBoxDemo()
		{
			var allBitmapDataDic:Dictionary=new Dictionary();

			//Prepare test data
			for (var i:int=0; i < 10; i++)
			{
				allBitmapDataDic["name_" + i]=new BitmapData(100, 100);
			}

			//Start Packing
			var builder:CEPackerBuilder=new CEPackerBuilder();
			builder.initialize(allBitmapDataDic, "DemoBox");
			builder.startPackData();

			//Finish, HavaFun!

			for each (var box:ICEPackerBox in builder.getAllPackerBoxVector())
			{
				trace("------Box: " + box.getBoxName() + " Size : " + box.getBoxSize());
				for each (var data:CEPackerElementData in box.getAllBoxElement())
				{
					trace("              " + data.bitmapDataName + "       " + data.bitmapDataRe.toString());
				}
				trace("\n");
			}
		}
	}
}
